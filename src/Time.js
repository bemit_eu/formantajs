/**
 * Converts a timestamp to a ISO8601 formatted time to display without a timezone
 *
 * @example timeToIso(Date.now())
 * @param timestamp
 * @return {string}
 */
function timeToIso(timestamp) {
    return timestamp.getFullYear() + '-'
        + (timestamp.getMonth() + '').padStart(2, '0') + '-'
        + (timestamp.getDate() + '').padStart(2, '0') + ' '
        + (timestamp.getHours() + '').padStart(2, '0') + ':'
        + (timestamp.getMinutes() + '').padStart(2, '0') + ':'
        + (timestamp.getSeconds() + '').padStart(2, '0')
}

export {timeToIso};