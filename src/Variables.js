function isUndefined(variable) {
    return 'undefined' === typeof variable;
}

function isString(variable) {
    return 'string' === typeof variable;
}

function isBool(variable) {
    return 'boolean' === typeof variable;
}

function isNumber(variable) {
    return 'number' === typeof variable;
}

function isObject(variable) {
    return 'object' === typeof variable;
}

function isFn(variable) {
    return 'function' === typeof variable;
}

function isSymbol(variable) {
    return 'symbol' === typeof variable;
}

function isEmptyObject(obj) {
    return Object.entries(obj).length === 0 && obj.constructor === Object;
}

/**
 *
 * @param {string} select a string like 'some.thing'
 * @param {object} obj an object containing data like {some:{ thing: mixed }}
 * @return {*}
 */
function selectByDot(select, obj) {
    return select.split('.').reduce(((o, i) => o ? o[i] : undefined), obj);
}

/**
 * Sets a value on the object property that gets selected with select.
 * @param select
 * @param {string} select a string like 'some.thing'
 * @param {object} obj an object containing data like {some:{ thing: mixed }}
 * @param {*} value
 */
function setByDot(select, obj, value) {
    const arr = select.split('.');
    let i = 0;
    arr.reduce(((o, k) => {
        if(arr.length === (i + 1)) {
            if(o) {
                o[k] = value;
            } else {
                o = {};
                o[k] = value;
            }
        } else if(!o[k]) {
            o[k] = {};
        }
        i++;
        return o[k];
    }), obj);
}

export {
    isUndefined, isBool, isString, isNumber, isObject, isFn, isSymbol,
    isEmptyObject,
    selectByDot, setByDot
};