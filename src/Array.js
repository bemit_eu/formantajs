function arrayFirst(arr) {
    for(let i in arr) return arr[i];
}

function sortString(arr, prop) {
    return arr.sort((a, b) => {
        let aProp = a[prop];
        let bProp = b[prop];
        if(aProp && !bProp) {
            return 1;
        }
        if(bProp && !aProp) {
            return -1;
        }
        if(isNaN(aProp)) {
            if(aProp) {
                aProp = aProp.toUpperCase();
            }
        } else {
            aProp = aProp * 1;
        }
        if(isNaN(bProp)) {
            if(bProp) {
                bProp = bProp.toUpperCase();
            }
        } else {
            bProp = bProp * 1;
        }
        return (aProp > bProp) ? 1 : ((bProp > aProp) ? -1 : 0);
    });
}

export {arrayFirst, sortString};