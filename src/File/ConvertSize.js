/**
 * Converts file sizes to human readable format
 *
 * @author by StackOverflow mpen https://stackoverflow.com/a/14919494/2073149
 * @param bytes
 * @param {boolean} si switch binary or decimal format
 * @return {string}
 */
const fileSize2Human = (bytes, si = true) => {
    let thresh = si ? 1000 : 1024;
    if(Math.abs(bytes) < thresh) {
        return bytes + ' B';
    }
    let units = si
        ? ['kB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB']
        : ['KiB', 'MiB', 'GiB', 'TiB', 'PiB', 'EiB', 'ZiB', 'YiB'];
    let u = -1;
    do {
        bytes /= thresh;
        ++u;
    } while(Math.abs(bytes) >= thresh && u < units.length - 1);
    return bytes.toFixed(1) + ' ' + units[u];
};

export {fileSize2Human};