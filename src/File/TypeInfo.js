const IMAGE = 'image';
const AUDIO = 'audio';
const VIDEO = 'video';
const DOCUMENT = 'document';
const ARCHIVE = 'archive';


const imageMimeTypes = [
    'image/jpeg',
    'image/png',
];

const audioMimeTypes = [
    'audio/mpeg',
    'audio/x-mpeg',
    'audio/mpeg3',
    'audio/x-mpeg-3',
    'audio/wav',
    'audio/x-wav',
];

const videoMimeTypes = [
    'video/mpeg',
    'video/x-mpeg',
    'video/mp2',
    'video/mp4',
    'video/quicktime',
];

const documentMimeTypes = [
    'text/plain',
    'application/msword',
    'application/excel',
    'application/x-excel',
    'application/x-msexcel',
    'application/pdf',
    'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
    'application/vnd.openxmlformats-officedocument.wordprocessingml.document',
];

const archiveMimeTypes = [
    'application/x-compressed',
    'application/x-zip-compressed',
    'application/zip',
    'multipart/x-zip',
];

const isImage = (mime) => {
    return (-1 !== imageMimeTypes.indexOf(mime));
};

const isAudio = (mime) => {
    return (-1 !== audioMimeTypes.indexOf(mime));
};

const isVideo = (mime) => {
    return (-1 !== videoMimeTypes.indexOf(mime));
};

const isDocument = (mime) => {
    return (-1 !== documentMimeTypes.indexOf(mime));
};

const isArchive = (mime) => {
    return (-1 !== archiveMimeTypes.indexOf(mime));
};

const getFileCategory = (mime) => {
    if(isImage(mime)) {
        return IMAGE;
    }

    if(isAudio(mime)) {
        return AUDIO;
    }

    if(isVideo(mime)) {
        return VIDEO;
    }

    if(isDocument(mime)) {
        return DOCUMENT;
    }

    if(isArchive(mime)) {
        return ARCHIVE;
    }

    return false;
};

export {isImage, isAudio, isVideo, isDocument, isArchive};
export {getFileCategory};
export {IMAGE, AUDIO, VIDEO, DOCUMENT, ARCHIVE};