import {arrayFirst, sortString} from './src/Array';
import {
    isUndefined, isBool, isString, isNumber, isObject, isFn, isSymbol,
    isEmptyObject,
    selectByDot, setByDot
} from "./src/Variables";
import {timeToIso} from "./src/Time";

import {fileSize2Human} from './src/File/ConvertSize';

import {isImage, isAudio, isVideo, isDocument, isArchive} from "./src/File/TypeInfo";
import {getFileCategory} from "./src/File/TypeInfo";
import {IMAGE, AUDIO, VIDEO, DOCUMENT, ARCHIVE} from "./src/File/TypeInfo";

import {ID} from "./src/ID";
import {onReady} from "./src/Ready";

export {
    // Array
    arrayFirst, sortString,
    // Variables
    isUndefined, isBool, isString, isNumber, isObject, isFn, isSymbol, isEmptyObject, selectByDot, setByDot,
    // Time
    timeToIso,
    // File Convert Size
    fileSize2Human,
    // File TypeInfo
    isImage, isAudio, isVideo, isDocument, isArchive,
    getFileCategory,
    IMAGE, AUDIO, VIDEO, DOCUMENT, ARCHIVE,
    // ID Generator
    ID,
    // onReady
    onReady
};